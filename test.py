import copy

a = [[1, 2, 3], [4, 5, 6]]
'''
This is shallow copy of the list and this will have the reference to the
already existing objects. Shallow copy can be performed by simply creating new
collection objects by calling the respective method like list(), dict(), set() 
'''
b = copy.copy(a)
print("The lists are {} and {}".format(a, b))

# just change an existing child object and see that the changes are reflected in the other list
a[0][1] = 'x'
print("The lists are {} and {}".format(a, b))

# But if a new child object is added then it won't be reflected in both the lists
a.append([7, 8, 9])
print("The lists are {} and {}".format(a, b))

'''
Deep copy will produce completely independent objects by 
recursively copying all the child objects also
'''
c = copy.deepcopy(a)
print("The lists are {} and {}".format(a, c))

# change an existing child object in a and check that it's not reflected in c
a[0][1] = 2
print("The lists are {} and {}".format(a, c))

s = [1, 2, 3]
s[:] = [4, 5, 6]
print(s)



